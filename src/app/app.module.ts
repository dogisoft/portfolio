import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule }   from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './components/main/app.component';
import { ContactComponent } from './components/contact/contact.component';
import { PricingComponent } from './components/pricing/pricing.component';
import { CompetenciesComponent } from './components/competencies/competencies.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { ReferencesComponent } from './components/references/references.component';
import { TechniquesComponent } from './components/techniques/techniques.component';


@NgModule({
  declarations: [
    AppComponent, CompetenciesComponent, ReferencesComponent, PricingComponent, ContactComponent, HomepageComponent, TechniquesComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    JsonpModule,
	BrowserAnimationsModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomepageComponent
      },
      {
        path: 'references',
        component: ReferencesComponent
      },
      {
        path: 'contact',
        component: ContactComponent
      },
      {
        path: 'pricing',
        component: PricingComponent
      },
      {
        path: 'techniques',
        component: TechniquesComponent
      },
      {
        path: 'competencies',
        component: CompetenciesComponent
      }
    ])
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
