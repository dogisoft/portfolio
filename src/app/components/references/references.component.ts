import { Component } from '@angular/core';

@Component({
	selector: 'references-root',
	templateUrl: './references.component.html',
	styleUrls: ['./references.component.css']
})

export class ReferencesComponent {	
	title = 'References';
	titles = ['humanoo.com, Front End Development', 'airx.aero / RWD Front End', 'coaching-kramann.de / Full Development','kreativmuhely.hu / Full Development',
	'hipavilon.hu / Full Development',
	'alexandra.hu / Redesign',
	'companisto.com / Front End Development',
	'biodiversityindex.org / Full Development',
	'bigidealive.com / Plugin Customization', 
	'chelseahire.co.uk / Plugin Customization', 
	'hunetkft.dogisoft.hu / Full Development (RWD)',
	'lovenorthampton.co.uk / Drupal dev'];
		
	refs = [
		{url: "humanoo.png", title: this.titles[0]},
		{url: "airx.png", title: this.titles[1]},
		{url: "kramann.png", title: this.titles[2]},
		{url: "kreativmuhely.png", title: this.titles[3]},
		{url: "hipavilon.png", title: this.titles[4]},
		{url: "alexandra.png", title: this.titles[5]},
		{url: "companisto.png", title: this.titles[6]},
		{url: "biodiversity.png", title: this.titles[7]},
		{url: "bigidealive.png", title: this.titles[8]},
		{url: "chelseahire.png", title: this.titles[9]},
		{url: "hunet.png", title: this.titles[10]},
		{url: "lovenorthampton.png", title: this.titles[11]}
	];
	

}
	




