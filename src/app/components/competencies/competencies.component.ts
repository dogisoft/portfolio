import { Component } from '@angular/core';

@Component({
  selector: 'competencies-root',
  templateUrl: './competencies.component.html',
  styleUrls: ['./competencies.component.css']
})

export class CompetenciesComponent {
	title = 'Homepage';
}
